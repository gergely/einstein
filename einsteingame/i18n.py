from locale import setlocale, LC_ALL, LC_NUMERIC

class Locale(object):
    def __init__(self):
        self.__country = None
        self.__language = None

        self.parseLocale(setlocale(LC_ALL, ""))
        # hack because of numbers in Lua
        setlocale(LC_NUMERIC, "C")

    @property
    def country(self):
        return self.__country

    @property
    def language(self):
        return self.__language

    def parseLocale(self, name):
        pos = name.find('.')

        langAndCountry = None

        if pos >= 0:
            encoding = name[pos + 1:]
            langAndCountry = name[0:pos]
        else:
            encoding = ""
            langAndCountry = name

        pos = langAndCountry.find('_')

        if pos < 0:
            self.__language = langAndCountry
            self.__country = ''
        else:
            self.__language = langAndCountry[0:pos]
            self.__country = langAndCountry[pos + 1:]

        self.__language = self.__language.lower()
        self.__country = self.__country.lower()
        encoding = encoding.upper()

def splitFileName(fileName):
    """
    Returns tuple of name, extension, lang, country
    """

    pos = fileName.rfind('.')

    if pos <= 0:
        ext = "";
        name = fileName
    else:
        name = fileName[0:pos]
        ext = fileName[pos + 1:]

    pos = name.rfind('_')

    if pos <= 0 or len(name) - pos != 3:
        lang = ""
        country = ""
    else:
        s = name[0:pos]
        l = name[pos + 1:]

        if l.isupper():
            name = s
            country = l
            pos = name.rfind('_')

            if pos <= 0 or len(name) - pos != 3:
                lang = ""
            else:
                l = name[pos + 1:]
                s = name[0:pos]

                if l.islower():
                    name = s
                    lang = l
                else:
                    lang = ""
        elif l.islower():
            name = s
            lang = l
            country = ""
        else:
            lang = ""
            country = ""

    return name, ext, lang, country

def getScore(lang, country, locale):
    if len(country) == 0 and len(lang) == 0:
        return 1

    score = 0

    if locale.country == country:
        score += 2

    if locale.language == lang:
        score += 4

    return score

locale = Locale()
