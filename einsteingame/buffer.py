class Buffer(object):
    def __init__(self):
        self.__size = 0
        self.__data = ''

    def setSize(self, size):
        self.__size = size

    def getData(self):
        return self.__data
