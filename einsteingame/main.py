#! /usr/bin/env python3

from random import randint
import os
import sys

import gi

gi.require_version('Clutter', '1.0')

from gi.repository import Clutter
from gi.repository import GLib

from .messages import msg
from .resources import ResourcesCollection, resources
from .utils import ensureDirExists

win32 = False
apple = False
developer = True

PREFIX = '/usr'

# stage = Clutter.Stage()
# stage.connect('destroy', lambda x: Clutter.main_quit())
# stage.title = 'Test'
# background = Clutter.Color()
# background.red, background.blue, background.green, background.alpha = 10, 10, 10, 255
# stage.set_background_color(background)
#
# actorRectangle = Clutter.Actor()
# actorRectangle.set_size(100, 100)
# actorRectangle.set_position(100, 100)
# rectColor = Clutter.Color()
# rectColor.red, rectColor.blue, rectColor.green, rectColor.alpha = 100, 100, 100, 255
# actorRectangle.set_background_color(rectColor)
#
# def changeRectanglePosition(x, y):
#     sizex, sizey = stage.get_size()
#     newx = randint(0, sizex)
#     newy = randint(0, sizey)
#     actorRectangle.set_position(newx, newy)
#
# actorRectangle.set_reactive(True)
# actorRectangle.connect('enter-event', changeRectanglePosition)
#
# stage.add_actor(actorRectangle)
# stage.show()
# Clutter.main()

stage = None
sound = None

def initScreen():
    Clutter.init()

    stage = Clutter.Stage()
    stage.connect('destroy', lambda x: Clutter.main_quit())
    stage.set_size(800, 600)
    stage.title = 'Einstein'

def initAudio():
    # sound = Sound()
    pass

def loadResources(selfPath):
    dirs = []

    if win32:
        dirs.append(getStorage().get("path", "") + "\\res")
    elif apple:
        dirs.append(getResourcesPath(selfPath))
    else:
        dirs.append(PREFIX + "/share/einstein/res")
        dirs.append(os.environ["HOME"] + "/.einstein/res")

    if developer:
        dirs.append(os.path.dirname(os.path.dirname(__file__)))

    dirs.append("res")
    dirs.append(".")
    resources = ResourcesCollection(dirs)
    msg.load(resources)

    return resources

def main(script, *args):
    ensureDirExists(os.environ["HOME"] + "/.einstein")

    resources = loadResources(sys.argv[0])
    initScreen()
    initAudio()
    # menu()
    # getStorage().flush()
